#include "draw.hpp"

u32 draw_curve(point g, curve c, std::ostream& output, double scale) {
	const auto to_x = [scale, c](u32 x) { return scale * ((x + (c.m) / 2u) % c.m + 1u); };
	const auto to_y = [scale, c](u32 y) { return scale * (1u + c.m - (y + c.m / 2u) % c.m); };
	const auto width = scale * (c.m + 2);
	output << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
	          "<svg xmlns=\"http://www.w3.org/2000/svg\" "
	          "xmlns:xlink=\"http://www.w3.org/1999/xlink\" "
	          "version=\"1.1\" baseProoutput=\"full\" "
	          "width=\""
	       << width << "\" height=\"" << width << "\">\n";

	output << "<polyline fill=\"none\" stroke=\"red\" stroke-width=\"2px\" points=\""
	       << to_x(g.x) << "," << to_y(g.y) << " ";
	auto p = g;
	auto points = u32{0};
	do {
		p = point_add(p, g, c);
		output << to_x(p.x) << "," << to_y(p.y) << " ";
		++points;
	} while (p != g);

	output << "\"/>\n"
	          "</svg>\n";
	return points;
}
