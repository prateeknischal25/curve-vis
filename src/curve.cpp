#include "curve.hpp"

#include <iomanip>
#include <istream>
#include <optional>
#include <ostream>

#include "mod_math.hpp"

bool operator==(point l, point r) { return std::tie(l.x, l.y) == std::tie(r.x, r.y); }

std::ostream& operator<<(std::ostream& s, const point& p) {
	return s << '(' << std::setw(2) << p.x << "," << std::setw(2) << p.y << ')';
}

std::istream& operator>>(std::istream& s, point& p) {
	auto c = char{};
	s >> p.x >> c >> p.y;
	if (c != ',') {
		s.setstate(std::ios_base::failbit);
	}
	return s;
}

point point_add(point lhs, point rhs, curve curve) {
	const auto m = curve.m;

	const auto inv = [m](u32 x) { return inv_m(x, m); };
	const auto add = [m](u32 l, u32 r) { return add_m(l, r, m); };
	const auto sub = [m](u32 l, u32 r) { return sub_m(l, r, m); };
	const auto mul = [m](u32 l, u32 r) { return mul_m(l, r, m); };

	const u32 prod = mul(mul(curve.d, mul(lhs.x, lhs.y)), mul(rhs.x, rhs.y));
	const u32 x = mul(add(mul(lhs.x, rhs.y), mul(lhs.y, rhs.x)), inv(add(1, prod)));
	const u32 y = mul(sub(mul(lhs.y, rhs.y), mul(lhs.x, rhs.x)), inv(sub(1, prod)));
	return {x, y};
}

point point_mul(point p, u32 x, curve curve) {
	auto ret = point{0u, 1u};
	auto factor = p;
	while(x > 0) {
		const auto b = x bitand 1u;
		x >>= 1u;
		if (b) {
			ret = point_add(ret, factor, curve);
		}
		factor = point_add(factor, factor, curve);
	}
	return ret;
}

bool on_curve(point p, curve c) {
	const auto add = [c](u32 l, u32 r) { return add_m(l, r, c.m); };
	const auto mul = [c](u32 l, u32 r) { return mul_m(l, r, c.m); };
	const auto square = [c](u32 i) { return square_m(i, c.m); };

	return (add(square(p.x), square(p.y))) ==
	       (add(1u, mul(c.d, mul(square(p.x), square(p.y)))));
}

std::optional<u32> get_y(curve c, u32 x) {
	const auto inv = [m = c.m](u32 x) { return inv_m(x, m); };
	const auto sub = [m = c.m](u32 l, u32 r) { return sub_m(l, r, m); };
	const auto mul = [m = c.m](u32 l, u32 r) { return mul_m(l, r, m); };
	const auto x2 = square_m(x, c.m);
	const auto y2 = mul(sub(x2, 1), inv(sub(mul(c.d, x2), 1)));
	return sqrt_m(y2, c.m);
}

point find_point(curve c) {
	auto x = u32{2};
	while (true) { // there is always some point
		const auto y = get_y(c, x);
		if (y != std::nullopt and on_curve({x, *y}, c)) {
			return {x, *y};
		}
		++x;
	}
}

std::vector<point> collect_points(curve c) {
	auto ret = std::vector<point>{};
	for (auto x = u32{}; x < c.m; ++x) {
		const auto y = get_y(c, x);
		if (y == std::nullopt) {
			continue;
		} else if (*y == 0) {
			ret.push_back({x, 0});
		} else {
			ret.push_back({x, *y});
			ret.push_back({x, c.m - *y});
		}
	}
	return ret;
}

u32 count_points(curve c) {
	auto ret = u32{};
	for (auto x = u32{}; x < c.m; ++x) {
		const auto y = get_y(c, x);
		if (y == std::nullopt) {
			continue;
		} else if (*y == 0) {
			++ret;
		} else {
			ret += 2u;
		}
	}
	return ret;
}

u32 point_order(point p, curve c) {
	auto ret = u32{};
	auto q = p;
	do {
		q = point_add(p, q, c);
		++ ret;
	} while (p != q);
	return ret;
}

