#include "primes.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <numeric>
#include <utility>

#include "mod_math.hpp"

namespace {

std::pair<u32, unsigned> prepare_n(u32 n) {
	auto j = 0u;
	while (n % 2u == 0 and n != 0) {
		++j;
		n >>= 1u;
	}
	return {n, j};
}

bool mr_round(u32 a, u32 d, unsigned j, u32 n) {
	auto p = pow_m(a, d, n);
	if (p == 1) {
		return true;
	}
	for (auto i = 0u; i < j; ++i) {
		if (p == n - 1) {
			return true;
		}
		p = square_m(p, n);
	}
	return false;
}

} // anonymous namespace

bool is_prime(u32 n) {
	if (n == 2 or n == 3) {
		return true;
	}
	if (n < 5) {
		return false;
	}
	const auto [d, j] = prepare_n(n - 1);
	if (n < 2047) {
		return mr_round(2, d, j, n);
	}
	for (auto a : {2u, 7u, 61u}) {
		if (not mr_round(a, d, j, n)) {
			return false;
		}
	}
	return true;
}

namespace {
u32 pollard_rho(u32 n) {
	assert(not is_prime(n));
	assert(n % 2u == 1u);
	const auto abs_diff = [](u32 x, u32 y) {
		if (x > y) {
			return x - y;
		} else {
			return y - x;
		}
	};
	for (auto i = u32{1}; i < n; ++i) {
		const auto f = [n, i](u32 x) { return mod(u64{x} * u64{x} + u64{i}, n); };
		auto x = u32{2};
		auto y = u32{2};
		auto d = u32{1};
		while (d == 1) {
			x = f(x);
			y = f(f(y));
			d = std::gcd(abs_diff(x, y), n);
		}
		if (d != n) {
			return d;
		}
	}
	throw std::logic_error{"factoring " + std::to_string(n) + " failed"};
}

// Yes, output-parameters suck, but so does pointless copying
void collect_factors(u32 n, std::vector<u32>& output) {
	if (is_prime(n)) {
		output.push_back(n);
		return;
	}
	const auto factor = pollard_rho(n);
	collect_factors(factor, output);
	collect_factors(n / factor, output);
}
} // anonymous namespace

std::vector<u32> factor(u32 n) {
	if (n < 4) {
		return {n};
	}
	auto ret = std::vector<u32>{};
	while (n % 2 == 0) {
		ret.push_back(2u);
		n /= 2u;
	}
	if (n != 1u) {
		collect_factors(n, ret);
		std::sort(ret.begin(), ret.end());
	}
	return ret;
}
