#ifndef TYPEDEFS_HPP
#define TYPEDEFS_HPP

#include <cstdint>

using u32 = std::uint32_t;
using u64 = std::uint64_t;

#endif
