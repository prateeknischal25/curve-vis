#ifndef DRAW_HPP
#define DRAW_HPP

#include <ostream>

#include "curve.hpp"

u32 draw_curve(point g, curve c, std::ostream& output, double scale = 1.0);

#endif
