#ifndef CURVE_HPP
#define CURVE_HPP

#include <istream>
#include <optional>
#include <ostream>
#include <vector>

#include "typedefs.hpp"

struct curve {
	u32 d;
	u32 m;
};

struct point {
	u32 x;
	u32 y;
};

bool operator==(point l, point r);

inline bool operator!=(point l, point r) { return !(l == r); }

std::ostream& operator<<(std::ostream& s, const point& p);

std::istream& operator>>(std::istream& s, point& p);

point point_add(point lhs, point rhs, curve curve);

point point_mul(point p, u32 x, curve curve);

bool on_curve(point p, curve c);

std::optional<u32> get_y(curve c, u32 x);

point find_point(curve c);

std::vector<point> collect_points(curve c);

u32 count_points(curve c);

u32 point_order(point p, curve c);

#endif
