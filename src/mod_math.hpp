#ifndef MOD_MATH_HPP
#define MOD_MATH_HPP

#include <optional>
#include <stdexcept>

#include "typedefs.hpp"

inline u32 mod(u64 n, u32 m) { return static_cast<u32>(n % m); }

inline u32 add_m(u32 a, u32 b, u32 m) { return mod(u64{a} + u64{b}, m); }

inline u32 sub_m(u32 a, u32 b, u32 m) { return mod(u64{m} + u64{a} - u64{b}, m); }

inline u32 mul_m(u32 a, u32 b, u32 m) { return mod(u64{a} * u64{b}, m); }

u32 pow_m(u32 base, u32 exp, u32 m);

inline bool is_square(u32 i, u32 m) { return pow_m(i, (m - 1) / 2u, m) == 1u; }

inline u32 square_m(u32 i, u32 m) { return mul_m(i, i, m); }

inline u32 inv_m(u32 i, u32 m) { return pow_m(i, m - 2u, m); }

std::optional<u32> sqrt_m(u32 i, u32 m);

#endif
