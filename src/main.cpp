#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include <clp/clp.hpp>

#include "curve.hpp"
#include "draw.hpp"
#include "primes.hpp"

std::string type_to_string(clp::parse_type<point>, int) { return "point"; }

std::ofstream open_file(const std::string& filename) {
	auto file = std::ofstream{filename};
	if (not file.is_open()) {
		throw std::runtime_error{"could not open file: " + filename};
	}
	return file;
}

void enforce(bool b, const std::string& msg) {
	if (not b) {
		throw std::runtime_error{msg};
	}
}

void print_factors(u32 n) {
	const auto factors = factor(n);
	std::cout << factors.front();
	for (auto i = 1u; i < factors.size(); ++i) {
		std::cout << " * " << factors[i];
	}
}

void list_d(u32 mod, bool all) {
	auto vec = std::vector<std::tuple<u32, u32, bool>>{};
	for (auto d = u32{2}; d < mod; ++d) {
		const auto count = count_points({d, mod});
		const auto good_count = (count % 4u == 0 and is_prime(count / 4u));
		if (all or good_count) {
			vec.push_back({d, count_points({d, mod}), good_count});
		}
	}
	// order by primality, order, d:
	const auto comp = [all](auto lhs, auto rhs) {
		const auto [lx, ln, l_prime] = lhs;
		const auto [rx, rn, r_prime] = rhs;
		const auto l = std::tuple{r_prime, rn, lx};
		const auto r = std::tuple{l_prime, ln, rx};
		return l > r;
	};
	std::sort(vec.begin(), vec.end(), comp);
	for (auto [d, c, prime] : vec) {
		std::cout << d << ": " << c << " = ";
		print_factors(c);
		std::cout << '\n';
	}
}

void list_points(curve c, bool all) {
	auto vec = std::vector<std::tuple<point, u32, bool>>{};
	for (auto p : collect_points(c)) {
		const auto order = point_order(p, c);
		const auto prime_order = is_prime(order);
		if (all or (prime_order and order != 2)) {
			vec.push_back({p, point_order(p, c), prime_order});
		}
	}
	const auto comp = [all](auto lhs, auto rhs) {
		const auto [pl, nl, prime_l] = lhs;
		const auto [pr, nr, prime_r] = rhs;
		const auto l = std::tuple{prime_r and nr != 2, nr, pl.x, pl.y};
		const auto r = std::tuple{prime_l and nl != 2, nl, pr.x, pr.y};
		return l > r;
	};
	std::sort(vec.begin(), vec.end(), comp);
	for (auto [p, c, prime] : vec) {
		std::cout << p << ": " << c;
		if (prime) {
			std::cout << " (prime)";
		} else {
			std::cout << " = ";
			print_factors(c);
		}
		std::cout << '\n';
	}
}

void list_subgroup(const point g, const curve c) {
	std::cout << g << '\n';
	for (auto p = point_add(g, g, c); p != g; p = point_add(p, g, c)) {
		std::cout << p << '\n';
	}
}

std::string gen_filename(std::string filename, curve c, point g) {
	if (filename == ".") {
		filename += '/';
	}
	if (filename.back() == '/') {
		filename += std::to_string(c.m) + "_" + std::to_string(c.d) + "d_" +
		            std::to_string(g.x) + "x_" + std::to_string(g.y) + "y.svg";
	}
	return filename;
}

const auto argument_parser = clp::arg_parser{
        clp::optional_argument<u32>{'m', "modulus",
                                    clp::description{"The prime whose field is used"}},
        clp::optional_argument<u32>{
                'd', "d", clp::dependencies{{"modulus"}},
                clp::description{"The curve-parameter “d”; must be greater than 1"}},
        clp::optional_argument<point>{'g', "generator", clp::dependencies{{"d"}},
                                      clp::description{"The generator; must lie on the curve"}},
        clp::optional_argument<std::string>{
                'o', "out", clp::dependencies{{"generator"}},
                clp::description{"The directory of file to which the curve will be "
                                 "written as SVG"}},
        clp::defaulted_argument<double>{
                's', "scale", 4.0, clp::dependencies{{"out"}},
                clp::description{"The factor by which the SVG will be scaled"}},
        clp::bool_flag{'c', "count", clp::dependencies{{"d"}},
                       clp::description{"Counts the points on the curve"}},
        clp::bool_flag{'l', "list-d", clp::dependencies{{"modulus"}},
                       clp::description{"List all possible curve-parameters and the size "
                                        "of the groups that they generate"}},
        clp::bool_flag{'p', "list-points", clp::dependencies{{"d"}},
                       clp::description{"List all points on the curve"}},
        clp::bool_flag{'a', "all", clp::description{"List all results"}},
        clp::bool_flag{'h', "help", clp::description{"Print this help and exit"}}};

int main(int argc, char** argv) try {
	const auto [mod, d, g, o, scale, c, l, p, a, h] = argument_parser.parse(argc, argv);
	if (h) {
		argument_parser.print_help(argv[0], std::cout);
		return 0;
	}
	enforce(mod != std::nullopt, "modulus required");
	const auto m = *mod;
	enforce(is_prime(m), std::to_string(m) + " is not a prime");
	if (l) {
		enforce(d == std::nullopt, "For listing d's, no value of d must be provided");
		list_d(m, a);
	}
	if (not(p or c or o)) {
		return 0;
	}
	enforce(d.has_value(), "operation requires d");
	const auto curve = ::curve{*d, m};
	enforce(g == std::nullopt or on_curve(*g, curve), "the generator has to lie on the curve");
	if (p and not g) {
		list_points(curve, a);
	}
	if (p and g) {
		list_subgroup(*g, curve);
	}
	if (c and not g) {
		std::cout << count_points(curve) << " points on the curve\n";
	}
	if (c and g and not o) {
		const auto n = point_order(*g, curve);
		std::cout << "The subgroup generated by " << *g << " contains " << n << " points\n";
	}
	if (o) {
		enforce(g.has_value(), "printing requires a generator");
		const auto filename = gen_filename(*o, curve, *g);
		auto file = open_file(filename);
		const auto points = draw_curve(*g, curve, file, scale);
		if (c) {
			std::cout << points << " points on the curve\n";
		}
	}
} catch (std::runtime_error& e) {
	std::cerr << "Error: " << e.what() << '\n';
	argument_parser.print_help(argv[0], std::cerr);
	return 1;
}
