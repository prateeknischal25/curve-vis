Curve-Vis - Visualizing Edwards-Curves since 2018
=================================================

It is widely known that elliptic curves are a valuable tool to provide
finite groups for cryptography. While visualizations of Weierstraß-curves
over ℝ² are readily available and pretty much everyone who is interested
has already seen them, they don't really provide an intuition why the
associated problem of point-division should be hard. This is similar
to how the logarithm on ℝ, doesn't provide a proper intuition for
why discrete logarithms might be hard.

Luckily both the English and the German Wikipedia have images of the
points of some elliptic curves over finite fields, but they still don't
provide the intuition of the chaos that these curves actually present.
Furthermore the used curves are all Weierstraß curves, which are not
really what we really want in cryptography (read as: “they really suck!”).
Montgomery-curves (like Curve25519) are better, but still share the
biggest issue with curves in general Weierstraß-form: They have a point
at infinity that requires special treatmend.

The solution here are Edwards-curves, which don't have that point, but
sadly lack a good definition of the group-law. In my opinion that is
however less important than being easy to implement correctly.

This leads to the purpose of this tool: It allows you to search for
untwisted Edwards-curves over finite fields, count and list their points
and to print the multiples of a certain generator.

It should be stressed that this is a tool that is purely for educational
purposes! The modulus of the underlying group is limited to 32-bit
integers which is way to small for **any** kind of real use. Furthermore
the implentation of counting points is by literally trying every possible
x-coordinate with brute-force and checking if there are points on it. (Which
of course implies that the program only works if the curve is so small that
even naive brute-force will break any discrete logarithm on it!)

Dependencies
------------

This tool only depends on [CLP](https://gitlab.com/FJW/clp), a
command-line-parser I've written which is header-only-library that has no
dependencies and never will. If this is still too much for you, you
can of course write your own main, but since part of the reason that
CLP is a dependency is testing CLP, I won't change this in the
official repo. ;-)

Usage
-----

This program supports the following options:

```
Usage: curve_vis [options]
        -m, --modulus      : integer      <optional>  The prime whose field is used
        -d, --d            : integer      <optional>  The curve-parameter “d”; must be greater than 1
        -g, --generator    : point        <optional>  The generator; must lie on the curve
        -o, --out          : string       <optional>  The directory of file to which the curve will be written as SVG
        -s, --scale        : float   = 4  <optional>  The factor by which the SVG will be scaled
        -c, --count        : flag    = 0  <optional>  Counts the points on the curve
        -l, --list-d       : flag    = 0  <optional>  List all possible curve-parameters and the size of the groups that they generate
        -p, --list-points  : flag    = 0  <optional>  List all points on the curve
        -r, --reverse-sort : flag    = 0  <optional>  List best results last
        -a, --all          : flag    = 0  <optional>  List all results
        -h, --help         : flag    = 0  <optional>  Print this help and exit
```

Some of these options prevent or require the use of others. The program
will tell you if this is the case.

The generator (`-g`) of type `point` has to be provided as comma-seperated
pair of integers (the x- and y-coordinates), like this: `-g '301,104'`.

A special note on the `--list-X`-arguments: These have the potential to be
very slow even for quite small values as they run in quadratic-time in
the modulus. Use them at your on disgression.

A valid example for usage is the following:

```
curve_vis -m307 -d130 -g '301,104' -o .
```

Results
-------

After all the preface, here is how a real, prime-order subgroup of an
Edwards curve over a prime-field looks:

![The Edwards curve given by x² + y² = 1 + 78x²y² over ℤ/499ℤ with (2, 94) as generator](docs/499_78d_2x_94y.svg)


